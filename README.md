## Setup, install dependencies.

Run in command line:

1. Install Python 3
2. Run in command line: `pip install pipenv`
3. Run in command line: `pipenv install`


## Environment variables.

In config folder create env configs in {env}.json config file. 

Create .env file
with ENV=configname in it. 


## Run tests locally.

- Create ".env" file with "ENV=dev" in the root directory.
- Create "dev.json" config file and place to src/configs/envs/
- Run all tests: `pipenv run pytest`

---
## Run tests locally with generating report 

- Run tests with creating results for allure report `pipenv run pytest tests --alluredir=allure_results`
- See report: `allure serve allure_results` or if you want to get a html report: `allure generate -c ./allure-results -o ./allure-report`
---