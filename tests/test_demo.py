import pytest

from src.services.functions import FunctionHelper


def test_1():
    assert FunctionHelper.add_two_numbers(1, 2) == 3, 'Error in adding two values'


def test_2():
    assert False, 'Assertion incorrect'


def test_3():
    assert 1 == 1, 'Not equal'


def test_4():
    assert 1 == 2, 'Not equal'


def test_6():
    assert 1 == 1, 'Not equal'


def test_7():
    assert 1 == 1, 'Not equal'


def test_8():
    assert 1 == 3, 'Not equal'


def test_9():
    assert 1 == 3, 'Not equal'


def test_10():
    raise Exception("some est exception")


def test_11():
    1 / 0


@pytest.mark.skip(reason="skiped due to")
def test_skipped():
    pass


@pytest.mark.xfail(reason="failed due to")
def test_failed():
    assert 1==2

