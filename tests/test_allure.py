import allure
import pytest


def test_1():
    assert 1 == 1


@allure.step
def fill_name(name):
    pass


@allure.step
def fill_password(param1, param2):
    pass


@allure.step
def click_button_login(elem):
    pass


@allure.feature("login feature")
@allure.title("Happy path for login")
@allure.description("this test verifies that valid user can log in")
@allure.severity(allure.severity_level.CRITICAL)
def test_login():
    fill_name("qwe")
    fill_password("123", "qwe")
    click_button_login("element")


@allure.feature("logout feature")
@allure.title("Happy path for logout")
@allure.description("this test verifies that valid user can log out")
@allure.severity(allure.severity_level.BLOCKER)
def test_logout():
    fill_name("asd")
    fill_password("asdasdasd", "asasdsasad")
    click_button_login("12312312")


@allure.feature("links feature")
@allure.title("attach links")
@allure.description("this test adds links")
@allure.link('https://yalantis.com/', name='Go to best IT company in the world')
@allure.testcase('https://yalantis.com/', 'TestCase1')
@allure.issue('https://yalantis.com/', 'Issue123')
def test_links():
    fill_name("asd")
    fill_password("asdasdasd", "asasdsasad")
    click_button_login("12312312")


@allure.feature("attachements feature")
@allure.title("attach files")
@allure.description("this test attaches files")
def test_attachements():
    fill_name("asd")
    allure.attach.file('tests/img.png', attachment_type=allure.attachment_type.PNG)
    allure.attach('<head></head><body> Hello HTML </body>', 'Attach with HTML type', allure.attachment_type.HTML)
    allure.attach('A text attachment in module scope fixture', 'blah blah blah', allure.attachment_type.TEXT)


@allure.title("Parameterized test title: param1 {param1} with param2 {param2}")
@allure.description("Test with two parameters and attachment")
@pytest.mark.parametrize('param1', [True, False])
@pytest.mark.parametrize('param2', ['value 1', 'value 2'])
def test_parametrize_with_two_parameters(param1, param2):
    fill_name(param1)
    fill_password(param1, param2)
    click_button_login(param1)


@allure.title("Parameterized with ids test title: param1 {param1} with param2 {param2}")
@allure.description("Test with two parameters and attachment")
@pytest.mark.parametrize('param1', [True, False], ids=['some value 1', 'some value 2'])
@pytest.mark.parametrize('param2', ['value 1', 'value 2'], ids=['some value param2 = 1', 'some value param2 = 2'])
def test_parametrize_with_two_parameters_with_ids(param1, param2):
    fill_name(param1)
    fill_password(param1, param2)
    click_button_login(param1)

